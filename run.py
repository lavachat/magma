# Magma: LavaChat reference server
# Copyright 2018, LavaChat Team and the Magma contributors
# SPDX-License-Identifier: AGPL-3.0-only

import sys
from pathlib import Path

from sanic import Sanic
from ruamel.yaml import YAML

from logbook import Logger, DEBUG, StreamHandler
from logbook.compat import redirect_logging

from magma.blueprints import (
    index, wellknown, startup, errors, websocket,
    users, signer, relationships
)

from magma.blueprints.auth import login, register

from magma.blueprints.s2s import (
    discovery as discovery_s2s,
    key_serving as keys_s2s
)

CONFIG_PATH = Path.cwd() / 'config' / 'config.yml'
SECRETS_PATH = Path.cwd() / 'config' / 'secrets.yml'

log = Logger('magma')


def _set_bp_list(app, blueprints: dict, base_prefix: str):
    for bpt, prefix in blueprints.items():
        url_prefix = f'{base_prefix}{prefix}' if prefix != 0 else None

        log.debug('blueprint: {}, url prefix: {}',
                  bpt.name, url_prefix)

        app.blueprint(bpt, url_prefix=url_prefix)


def _set_blueprints(app):
    """Load all app blueprints."""
    # instead of keeping listener() calls for things like database
    # conn initialization in run.py (that wouldn't make much sense,
    # since we don't have a global app object),
    # we move all startup-related code
    # to its own blueprint, magma/blueprints/startup.py

    blueprints = {
        startup: 0,
        errors: 0,
        signer: 0,

        index: 0,
        wellknown: 0,

        login: '/auth/login',
        register: '/auth',

        websocket: '',
        users: '/users',
        relationships: '/relationships',
    }
    _set_bp_list(app, blueprints, '/api')

    s2s_blueprints = {
        discovery_s2s: '',
        keys_s2s: '/keys'
    }
    _set_bp_list(app, s2s_blueprints, '/_/lavachat')



def make_app():
    """Generate the main app object."""
    app = Sanic()

    yaml = YAML(typ='safe')
    conf = yaml.load(CONFIG_PATH)

    app.config.update(conf)

    handler = StreamHandler(sys.stdout)
    handler.push_application()

    redirect_logging()

    if app.config.debug:
        handler.level = DEBUG

    try:
        yaml_keys = YAML(typ='safe')
        secrets_conf = yaml_keys.load(SECRETS_PATH)
        app.config['secrets'] = secrets_conf
    except FileNotFoundError:
        log.warning('no secrets file found, please generate')

    # for the more complex startup stuff
    # such as databases and singletons such as
    # Storage, look over magma/blueprints/startup.
    _set_blueprints(app)

    return app


def main():
    """Main entrypoint."""
    app = make_app()

    app.run(host=app.config['server']['host'],
            port=app.config['server']['port'])

if __name__ == '__main__':
    main()
