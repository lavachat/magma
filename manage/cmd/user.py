# Magma: LavaChat reference server
# Copyright 2018, LavaChat Team and the Magma contributors
# SPDX-License-Identifier: AGPL-3.0-only

from asyncpg import UniqueViolationError

from magma.common.user import register_user
from tests.creds import CREDS

async def register(ctx, args):
    user_id = await register_user(
        ctx, args.username, args.email, args.password
    )

    print('new user id', user_id)


async def setup_test_users(ctx, _args):
    """Setup example test users."""

    for internal_id, auth_tup in CREDS.items():
        username, password = auth_tup

        try:
            uid = await register_user(
                ctx, username, 'a@a.com', password
            )
        except UniqueViolationError:
            print('test user', repr(internal_id), 'already setup')
            continue

        print('setup test user', repr(internal_id))
        print('\tuid:', uid)

    print('OK')


def setup(subparser):
    register_parser = subparser.add_parser('register', help='create a user')

    register_parser.add_argument(
        'username', help='username of the user')
    register_parser.add_argument(
        'email', help='email of the user')
    register_parser.add_argument(
        'password', help='password of the user')

    register_parser.set_defaults(func=register)

    test_parser = subparser.add_parser(
        'setup_tests',
        help='setup test users'
    )

    test_parser.set_defaults(func=setup_test_users)
