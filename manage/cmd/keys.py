# Magma: LavaChat reference server
# Copyright 2018, LavaChat Team and the Magma contributors
# SPDX-License-Identifier: AGPL-3.0-only

from cryptography.hazmat.backends import default_backend
from cryptography.hazmat.primitives.asymmetric import rsa
from cryptography.hazmat.primitives.serialization import (
    Encoding, PrivateFormat, NoEncryption
)

from ruamel.yaml import YAML

from run import SECRETS_PATH

async def genkeys(_ctx, _args):
    """Generate keys."""
    if SECRETS_PATH.exists():
        print('secrets file exists, use --force to regen keys')
        print('note: everything might break with a regen.')
        return

    private_key = rsa.generate_private_key(
        public_exponent=65537,
        key_size=2048,
        backend=default_backend()
    )

    private_bytes = private_key.private_bytes(
        encoding=Encoding.PEM,
        format=PrivateFormat.PKCS8,
        encryption_algorithm=NoEncryption()
    )

    yaml = YAML(typ='safe')
    yaml.default_flow_style = False
    yaml.dump({
        'signing_key': private_bytes.decode()
    }, SECRETS_PATH)

    print('OK')



def setup(subparser):
    """Setup commands"""
    genkeys_p = subparser.add_parser(
        'genkeys', help='generate keys for lavachat')

    genkeys_p.add_argument(
        '--force',
        help='force key generation',
        action='store_true')

    genkeys_p.set_defaults(func=genkeys)
