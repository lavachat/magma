# Magma: LavaChat reference server
# Copyright 2018, LavaChat Team and the Magma contributors
# SPDX-License-Identifier: AGPL-3.0-only

import os
import sys
import asyncio
from argparse import ArgumentParser

from logbook import Logger

# hacky!
sys.path.append(os.getcwd())

from run import make_app

from magma.blueprints.startup import setup_db, close_app
from manage.cmd import user, keys


log = Logger(__name__)


def _get_parser() -> ArgumentParser:
    parser = ArgumentParser()
    subparser = parser.add_subparsers(help='operation')

    user(subparser)
    keys(subparser)

    return parser


def main():
    """Main manage script function."""
    loop = asyncio.get_event_loop()

    app = make_app()
    loop.run_until_complete(setup_db(app))

    parser = _get_parser()

    args = sys.argv

    if len(args) < 2:
        parser.print_help()
        return

    args = parser.parse_args()

    try:
        loop.run_until_complete(args.func(app, args))
    except:
        log.exception('error while running command')
    finally:
        loop.run_until_complete(close_app(app))
