#!/usr/bin/env python3
# Magma: LavaChat reference server
# Copyright 2018, LavaChat Team and the Magma contributors
# SPDX-License-Identifier: AGPL-3.0-only

import sys

from manage.main import main

if __name__ == '__main__':
    sys.exit(main())
