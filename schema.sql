CREATE TABLE IF NOT EXISTS users (
    -- contains a FULL UserID for both local and external users.

    -- NOTE: while this approach is inefficient since we'll need
    -- to store the instance domain all the time, it gives us
    -- simplicity when writing queries. the old approach was leaving
    -- this NULL for local users and that fucked the queries over
    -- with all sorts of hacks and f-strings to account for this column.
    id text PRIMARY KEY,

    -- uuid for local users, NULL for externals
    local_id uuid UNIQUE,

    -- the username to login, NULL for externals
    username text UNIQUE,

    -- the user's preferred "showing name"
    nickname text,

    -- other things
    bot boolean DEFAULT FALSE,

    -- owo private information, NULL for externals
    email text UNIQUE,
    verified boolean DEFAULT FALSE,
    password_hash text
);

CREATE TABLE IF NOT EXISTS instance_keys (
    -- instance the key belongs to, only
    -- one key per instance
    instance text PRIMARY KEY,
    key_type text,
    key text,

    -- unix timestamp
    expires_at bigint
);

-- relationship things
CREATE TABLE IF NOT EXISTS relationships (
    user_id text REFERENCES users (id),
    peer_id text REFERENCES users (id),

    -- 0 for incoming friend requests
    -- 1 for outgoing friend requests
    -- 2 for friends :)
    -- 3 for blocks :(
    rel_type SMALLINT,

    PRIMARY KEY (user_id, peer_id)
);

CREATE VIEW direct_relationships as
    SELECT user_id, peer_id, rel_type
    FROM relationships
    WHERE rel_type IN (3, 4);

CREATE VIEW friend_requests as
    SELECT user_id, peer_id, rel_type
    FROM relationships
    WHERE rel_type IN (0, 1);
