# Magma: LavaChat reference server
# Copyright 2018, LavaChat Team and the Magma contributors
# SPDX-License-Identifier: AGPL-3.0-only

from sanic import Blueprint
from logbook import Logger

from magma.response import json
from magma.errors import BadRequest
from magma.decorators import auth_route
from magma.enums import RelationshipType, RawRelType
# from magma.utils import split_user_id
# from magma.schemas import validate, UserSchemas

bp = Blueprint(__name__)
log = Logger(__name__)


@bp.get('/@me')
@auth_route
async def get_my_relationships(request, user_id):
    """Get your relationships."""
    return json(
        await request.app.storage.user.get_relationships(user_id)
    )


@bp.get('/@me/requests')
@auth_route
async def get_my_rel_reqs(request, user_id):
    """Get relationship requests."""
    return json(
        await request.app.storage.user.get_rel_reqs(user_id)
    )

@bp.put('/@me/requests/<peer_id>')
@auth_route
async def create_friend_request(request, user_id, peer_id):
    """Create a friend request to another user (the peer)"""

    # TODO: finish this
    try:
        RelationshipType(request.json['type'])
    except (KeyError, ValueError):
        raise BadRequest('Invalid relationship type')

    app = request.app

    # first, we need to check if we already have a request from the
    # peer to us, and if it does, we just create it as a friendship.
    req = await app.db.fetchrow(f"""
    SELECT peer_id
    FROM friend_requests
    WHERE peer_id = $1 AND user_id = $2 AND rel_type = $3
    """, peer_id, user_id, RawRelType.incoing_friend.value)

    # if there is an incoming friend request from the peer
    # and we want to make a friend request to the peer,
    # then we are pratically accepting that friend request.
    if req is not None:
        # await create_relationship(
        #    app, user_id, peer_id, RawRelType.friend)
        raise NotImplementedError('unimplemented')

    # new friend request, insert it into db
    # and federate it out to the peer
    raise NotImplementedError('unimplemented')
