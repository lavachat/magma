# Magma: LavaChat reference server
# Copyright 2018, LavaChat Team and the Magma contributors
# SPDX-License-Identifier: AGPL-3.0-only

import base64
import hashlib
import datetime
from email.utils import formatdate
from time import mktime

from sanic import Blueprint
from logbook import Logger

from httpsig import HeaderSigner

bp = Blueprint(__name__)
log = Logger(__name__)


def _make_signer(app) -> HeaderSigner:
    """Make a HeaderSigner instance based off the
    main signing private key."""
    sign_key = app.storage.keys.self_signing_pem
    inst_url = app.config.magma['url']

    return HeaderSigner(
        #: key id
        f'https://{inst_url}/_/lavachat/keys/@self',
        sign_key,
        algorithm='rsa-sha256',
        headers=['(request-target)', 'host', 'date', 'digest']
    )


def hash_digest(hash_constructor, data) -> str:
    """Hash any given data, given the hash_constructor, and return the result
    as a base64 encoded string."""
    return base64.b64encode(
        hash_constructor(data).digest()
    ).decode()


def gen_digests(body) -> str:
    """Generate values for the Digest header, given body.

    Digest header is defined in RFC3230,
    https://tools.ietf.org/html/rfc3230

    Parameters
    ----------
    body: Union[str, bytes]
        The request/response body to generate digests for.

    Returns
    -------
    str
        The value to set in the Digest header.
    """
    try:
        body = body.encode()
    except AttributeError:
        pass

    sha256_hash = hash_digest(hashlib.sha256, body)
    sha512_hash = hash_digest(hashlib.sha512, body)
    return f'SHA-256={sha256_hash} SHA-512={sha512_hash}'


def mk_date_header() -> str:
    """Generate a valid value for the Date header."""
    now = datetime.datetime.now()
    tstamp = mktime(now.timetuple())
    return formatdate(
        timeval=tstamp, localtime=False, usegmt=True
    )


@bp.middleware('response')
async def signer_middleware(request, response):
    """Add a signature to a response.

    This does NOT sign responses that aren't flagged correctly.
    """

    # there is also the option where the response does not have
    # sign_flag at all. if it doesn't, assume the response is NOT
    # to be signed.
    try:
        if not response.sign_flag:
            return
    except AttributeError:
        return

    app = request.app

    # check if signer is in memory, if not, make it
    try:
        app.signer
    except AttributeError:
        app.signer = _make_signer(app)

    signer = app.signer

    # use the host header
    # TODO: maybe we use the magma.url config parameter?
    response.headers['host'] = request.headers['host']
    response.headers['date'] = mk_date_header()
    response.headers['digest'] = gen_digests(response.body)

    # the signer just automatically puts the signature on the
    # authorization header, so we copy that over to the
    # actual signature header, used by us.
    signature = signer.sign(
        response.headers, method=request.method, path=request.path)

    response.headers['signature'] = signature['authorization']
