# Magma: LavaChat reference server
# Copyright 2018, LavaChat Team and the Magma contributors
# SPDX-License-Identifier: AGPL-3.0-only

from sanic import Blueprint
from logbook import Logger

from magma.response import json
from magma.storage.keys import to_external_pub

bp = Blueprint(__name__)
log = Logger(__name__)


@bp.get('/@self')
async def give_self_keys(request):
    """Give our public key information."""
    app = request.app

    signing_key = app.storage.keys.self_signing_pub
    to_send = to_external_pub(signing_key)

    return json({
        # NOTE: no idea about a proper TTL
        'ttl': 1800,
        'key': to_send
    })


@bp.get('/<other>')
async def give_other_keys(request, other: str):
    """Give the keys for another instance, if cached."""
    app = request.app
    await app.federator.is_blocked(other)
    other_key = await app.storage.keys.get_instance_key(other)
    return json(other_key)
