# Magma: LavaChat reference server
# Copyright 2018, LavaChat Team and the Magma contributors
# SPDX-License-Identifier: AGPL-3.0-only

from sanic import Blueprint
from logbook import Logger

from magma.response import json
from magma.errors import BadRequest
from magma.decorators import s2s_in
from magma.blueprints.users import search_uuid, search_alias


bp = Blueprint(__name__)
log = Logger(__name__)


@bp.get('/discovery')
@s2s_in
async def s2s_discovery(request, _peer_domain):
    """Called by other servers to know about an alias in this server."""

    try:
        query = request.args['q'][0]
    except (KeyError, IndexError):
        raise BadRequest('missing query parameters')

    app = request.app

    _func = search_uuid if ':' in query else search_alias
    users = await _func(app, query)

    return json({
        'users': users
    })
