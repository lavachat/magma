# Magma: LavaChat reference server
# Copyright 2018, LavaChat Team and the Magma contributors
# SPDX-License-Identifier: AGPL-3.0-only

from .discovery import bp as discovery
from .key_serving import bp as key_serving

__all__ = ['discovery', 'key_serving']
