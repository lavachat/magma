# Magma: LavaChat reference server
# Copyright 2018, LavaChat Team and the Magma contributors
# SPDX-License-Identifier: AGPL-3.0-only

from .index import bp as index
from .wellknown import bp as wellknown
from .startup import bp as startup
from .errors import bp as errors
from .websocket import bp as websocket
from .users import bp as users
from .signer import bp as signer
from .relationships import bp as relationships

__all__ = ['index', 'wellknown', 'startup', 'errors',
           'websocket', 'users', 'signer', 'relationships']
