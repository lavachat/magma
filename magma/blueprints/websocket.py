# Magma: LavaChat reference server
# Copyright 2018, LavaChat Team and the Magma contributors
# SPDX-License-Identifier: AGPL-3.0-only

from sanic import Blueprint

from magma.websocket.connection import WSConnection
from magma.websocket.types import Encoding, Compression

bp = Blueprint(__name__)

@bp.websocket('/ws')
async def main_websocket(request, ws):
    """Main websocket handler. Spawns a WSConnection object."""
    encoding = request.args.get('encoding', 'json')
    compression = request.args.get('compression', None)

    encoding = Encoding(encoding)
    compression = Compression(compression)

    conn = WSConnection(request.app, ws, encoding, compression)
    await conn.loop()
