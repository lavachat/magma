# Magma: LavaChat reference server
# Copyright 2018, LavaChat Team and the Magma contributors
# SPDX-License-Identifier: AGPL-3.0-only

import asyncpg
from sanic import Blueprint
from logbook import Logger

# import singletons
from magma.storage import Storage
from magma.s2s.federator import Federator
from magma.websocket.state_manager import StateManager

bp = Blueprint(__name__)
log = Logger(__name__)


def _set_singletons(app):
    app.storage = Storage(app)
    app.federator = Federator(app)
    app.state_manager = StateManager(app)


async def setup_db(app):
    """Initialize Postgres connection."""
    log.info('connecting to db')
    app.db = await asyncpg.create_pool(**app.config.postgres)

    _set_singletons(app)


async def close_app(app):
    """Close functions for the app."""
    await app.db.close()
    await app.federator.session.close()
    await app.storage.keys.session.close()



@bp.listener('before_server_start')
async def setup_app(app, _loop):
    """Setup common app resources, such as Postgres."""
    await setup_db(app)


@bp.listener('after_server_stop')
async def stop_app(app, _loop):
    await close_app(app)
