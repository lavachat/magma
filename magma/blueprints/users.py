# Magma: LavaChat reference server
# Copyright 2018, LavaChat Team and the Magma contributors
# SPDX-License-Identifier: AGPL-3.0-only

from sanic import Blueprint
from logbook import Logger

from magma.response import json
from magma.errors import BadRequest, UserNotFound
from magma.decorators import auth_route
from magma.utils import split_user_id
from magma.schemas import validate, UserSchemas
from magma.common.auth import hash_data

bp = Blueprint(__name__)
log = Logger(__name__)


async def search_uuid(app, query: str, *, local: bool = False):
    """Search a user via UUID.

    Parameters
    ----------
    app: sanic.App
        app instance holding federator instance
    query: str
        a full UserID (@uuid:instance.tld).
    local: bool, optional
        if you only want to search local users and NOT delegate
        the query to the federator. default False.

    Returns
    -------
    List[User]
        Contains a single user.
    """
    # check if we have it in db beforehand
    try:
        user = await app.storage.user.get(query)
        return [user]
    except UserNotFound:
        pass

    # if we don't have it in our database, we do an external query
    # via the federator.
    _uuid, instance = split_user_id(query)

    if not local:
        return await app.federator.user_discovery(instance, query)

    return []


async def search_alias(app, query: str, *, local: bool = False) -> list:
    """Search users via their UserAlias.

    Parameters
    ----------
    app: sanic.App
        app instance holding federator instance
    query: str
        a UserAlias (something@instance.tld).
    local: bool, optional
        if you only want to search local users and NOT delegate
        the query to the federator. default False.

    Returns
    -------
    List[User]
        Users that matched the query. Can be one or many.
    """
    # as with search_uuid, try the database first
    try:
        user = await app.storage.user.get(query)
        return [user]
    except UserNotFound:
        pass

    # default the instance url to ours when
    # the given uid doesnt match up.
    try:
        _, instance = query.split('@')
    except ValueError:
        instance = app.config.magma['url']

    if not local:
        return await app.federator.user_discovery(instance, query)

    return []


@bp.get('/@me')
@auth_route
async def _users_me(request, user_id):
    app = request.app
    return json(
        await app.storage.user.get_private(user_id)
    )


@bp.patch('/@me')
@auth_route
async def _patch_users_me(request, user_id):
    app = request.app
    profile = validate(request.json, UserSchemas.PROFILE)

    for field, value in profile.items():

        # if we are updating password, make a hash of it, etc
        if field == 'password':
            field = 'password_hash'
            value = await hash_data(value)

        # TODO: email verification, that'd be in the long future.

        await app.db.execute(f"""
            UPDATE users
            SET {field} = $1
            WHERE id = $2
        """, value, user_id)

    return json(
        await app.storage.user.get_private(user_id)
    )


@bp.get('/<user_id>')
@auth_route
async def _get_other_user(request, _actor_id, user_id: str):
    """Get another user via their UserID."""
    app = request.app
    return json(
        await app.storage.user.get(user_id)
    )


@bp.get('/search')
@auth_route
async def search_users(request, _user_id):
    """Search users on the network."""
    try:
        query = request.args['q'][0]
    except KeyError:
        raise BadRequest('missing query parameter')

    # query is Union[UserAlias, UserID], treat accordingly.
    app = request.app

    _func = search_uuid if ':' in query else search_alias
    users = await _func(app, query)
    return json(users)
