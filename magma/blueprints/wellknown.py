# Magma: LavaChat reference server
# Copyright 2018, LavaChat Team and the Magma contributors
# SPDX-License-Identifier: AGPL-3.0-only

from sanic import Blueprint
from sanic.response import json

bp = Blueprint(__name__)


@bp.get('/.well-known/lavachat')
async def index(request):
    """Give base URLs and protocol version."""
    cfg = request.app.config
    magma_cfg = cfg.magma

    # for now, v0
    ver = 0

    return json({
        'c2s_apis': {
            f'magma_api_{ver}': f'{magma_cfg["api_url"]}/api',
            f'magma_api_ws_{ver}': f'{magma_cfg["ws_url"]}'
        },
        's2s_apis': {
            f'lavachat_{ver}': f'{magma_cfg["api_url"]}/_/lavachat'
        }
    })
