# Magma: LavaChat reference server
# Copyright 2018, LavaChat Team and the Magma contributors
# SPDX-License-Identifier: AGPL-3.0-only

from .login import bp as login
from .register import bp as register

__all__ = ['login', 'register']
