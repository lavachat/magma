# Magma: LavaChat reference server
# Copyright 2018, LavaChat Team and the Magma contributors
# SPDX-License-Identifier: AGPL-3.0-only

from sanic import Blueprint
from magma.response import json

from magma.errors import MethodNotAllowed
from magma.schemas import validate, AuthSchemas

from magma.common.user import register_user

bp = Blueprint(__name__)


@bp.post('/register')
async def register_new_user(request):
    """Register a single user."""
    app = request.app
    cfg = request.app.config

    if not cfg.auth['registrations']:
        raise MethodNotAllowed('Registrations disabled')

    j = validate(request.json, AuthSchemas.REGISTER)

    user_id = await register_user(
        app, j['username'], j['email'], j['password']
    )

    return json({
        'id': user_id
    })
