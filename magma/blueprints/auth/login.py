# Magma: LavaChat reference server
# Copyright 2018, LavaChat Team and the Magma contributors
# SPDX-License-Identifier: AGPL-3.0-only

from sanic import Blueprint
from sanic.response import json
from itsdangerous import TimestampSigner

from magma.schemas import validate, AuthSchemas
from magma.errors import AuthFailure
from magma.common.auth import hash_check
from magma.utils import split_user_id

bp = Blueprint(__name__)


def make_token(user_id: str, pwd_hash) -> str:
    """Generate a single token for a given user id and
    its password hash."""
    signer = TimestampSigner(pwd_hash)
    uuid, _ = split_user_id(user_id)
    return signer.sign(uuid).decode()


@bp.post('/plain')
async def login_plaintext(request):
    """Login plaintext via giving the user and password."""
    j = validate(request.json, AuthSchemas.LOGIN_PLAIN)
    app = request.app

    row = await app.db.fetchrow("""
    SELECT id, password_hash
    FROM users
    WHERE username = $1
    """, j['username'])

    if row is None:
        raise AuthFailure('User not found')

    uid, phash = row

    await hash_check(phash, j['password'])

    return json({
        'token': make_token(uid, phash)
    })


@bp.post('/scram')
async def login_scram(request):
    # SCRAM works via 4 messages, maybe we can use streaming here.
    # maybe even a websocket.
    return json({})


@bp.post('/totp')
async def login_totp_stage(request):
    return json({})
