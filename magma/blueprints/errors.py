# Magma: LavaChat reference server
# Copyright 2018, LavaChat Team and the Magma contributors
# SPDX-License-Identifier: AGPL-3.0-only

from sanic import Blueprint
from sanic.response import json

from logbook import Logger

from magma.errors import MagmaError

bp = Blueprint(__name__)
log = Logger(__name__)


@bp.exception(MagmaError)
async def _handle_err(_request, exception):
    status_code = exception.status_code

    log.debug('magma error: {!r}', exception)

    try:
        message = exception.args[0]
    except IndexError:
        message = repr(exception)

    base = {
        'error': True,
        'message': message
    }

    try:
        base.update(exception.args[1])
    except IndexError:
        pass

    return json(base, status=status_code)


@bp.exception(Exception)
async def _handle_exc(request, exception):
    log.exception('error in request')

    return json({
        'error': True,
        'unexpected': True,
        'message': repr(exception)
    }, status=500)
