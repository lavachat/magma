# Magma: LavaChat reference server
# Copyright 2018, LavaChat Team and the Magma contributors
# SPDX-License-Identifier: AGPL-3.0-only
from enum import IntEnum


class RelationshipType(IntEnum):
    """Represents a relationship"""
    friend = 0
    block = 1


class FriendRequestType(IntEnum):
    """Represents the type of a friendship request."""
    incoming = 0
    outgoing = 1


class RawRelType(IntEnum):
    """Represents raw integer types for relationships."""
    incoming_friend = 0
    outgoing_friend = 1
    friend = 2
    block = 3
