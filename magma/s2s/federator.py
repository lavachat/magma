# Magma: LavaChat reference server
# Copyright 2018, LavaChat Team and the Magma contributors
# SPDX-License-Identifier: AGPL-3.0-only

import json

import aiohttp
from logbook import Logger
from httpsig import HeaderSigner, HeaderVerifier

from magma.errors import (
    ExternalConnectionError, SignatureError, PeerBlocked
)

from magma.blueprints.signer import gen_digests, mk_date_header

log = Logger(__name__)


class Federator:
    """Class for federation / s2s."""
    BASE_PATH = '/_/lavachat'

    def __init__(self, app):
        self.app = app
        self.session = aiohttp.ClientSession()
        self._signer = None

    @property
    def signer(self):
        """Get a signer instance for our outgoing requests."""
        if self._signer is None:
            # generate a signer which is different from the one
            # for the response.
            inst_url = self.app.config.magma['url']
            sign_key = self.app.storage.keys.self_signing_pem

            self._signer = HeaderSigner(
                f'https://{inst_url}/_/lavachat/keys/@self',
                sign_key,
                algorithm='rsa-sha256',
                headers=['(request-target)', 'host', 'date', 'digest',
                         'x-lavachat-origin']
            )

        return self._signer

    def mk_headers(self, instance: str, method: str,
                   path: str, headers: dict, kwargs: dict):
        """Make headers for the request."""

        headers['digest'] = gen_digests(
            json.dumps(kwargs.get('json', ''))
        )
        headers['host'] = instance
        headers['date'] = mk_date_header()
        headers['x-lavachat-origin'] = self.app.config.magma['url']

        signature = self.signer.sign(
            headers, method=method, path=path
        )

        headers['signature'] = signature['authorization']

    async def check_signature(self, resp, method, path):
        """Check the signature in a response object."""
        try:
            resp.headers['signature']
        except KeyError:
            raise SignatureError('no signature provided')

        peer_domain = resp.headers['host']
        peer_key = await self.app.storage.keys.get_instance_key(peer_domain)

        verifier = HeaderVerifier(
            resp.headers, peer_key['key'], method=method, path=path,
            host=peer_domain, sign_header='signature'
        )

        if not verifier.verify():
            raise SignatureError('verification failed')

    async def _request(self, instance: str, method: str, path: str,
                       *args, **kwargs):
        """Make an outgoing request to another Server.

        This generates a signature for the request and checks the
        signature for the response given by the other Server.
        """
        #: first, check if the outgoing instance is blocked *by us*
        await self.is_blocked(instance)

        # generate the request path
        path = f'{self.BASE_PATH}{path}'
        url = f'https://{instance}{path}'

        headers = kwargs.get('headers', {})
        try:
            kwargs.pop('headers')
        except KeyError:
            pass

        log.debug('ext req: instance={} method={}, path={}',
                  instance, method, path)

        # sign our headers!
        self.mk_headers(instance, method, path, headers, kwargs)

        try:
            async with self.session.request(
                method, url, headers=headers, *args, **kwargs) as resp:
                rjson = await resp.json()
                await self.check_signature(resp, method, path)

                # TODO: parse the results of rjson into
                # meaningful errors.
                return rjson
        except (aiohttp.ClientError,
                aiohttp.ContentTypeError):
            log.exception('client error')
            raise ExternalConnectionError('Failed to s2s.')

    async def user_discovery(self, instance: str, query: str):
        """Discover a user in another server.

        Parameters
        ----------
        instance: str
            The Server to make the request to.
        query: str
            Query parameters for the user.
        """
        # TODO: check if instance == config.magma['inst_url']
        # and if it is, pass to its own handler

        users = await self._request(instance, 'GET', '/discovery', params={
            'q': query
        })

        # TODO: validate

        # for each discovered user, insert it into our database
        for user in users:
            await self.app.storage.user.insert_external(user)

        return users

    async def is_blocked(self, instance: str, *, raise_err=True) -> bool:
        """Return if an external instance is blocked."""
        blocked = self.app.config.federation['blocked_servers']
        is_blocked = instance in blocked

        if is_blocked and raise_err:
            raise PeerBlocked('Peer server blocked.')

        return is_blocked
