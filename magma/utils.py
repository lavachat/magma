# Magma: LavaChat reference server
# Copyright 2018, LavaChat Team and the Magma contributors
# SPDX-License-Identifier: AGPL-3.0-only

def split_user_id(user_id: str) -> tuple:
    """Split a full UserID into its UUID and instance URL parts."""
    almost_uuid, instance = user_id.split(':')
    uuid = almost_uuid.strip('@')
    return uuid, instance
