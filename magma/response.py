# Magma: LavaChat reference server
# Copyright 2018, LavaChat Team and the Magma contributors
# SPDX-License-Identifier: AGPL-3.0-only

import json as pyjson
import uuid
from typing import Any

from sanic import response


class CustomJSONEncoder(pyjson.JSONEncoder):
    def default(self, value: Any):
        if isinstance(value, uuid.UUID):
            return value.hex

        return super().default(value)


def json(*args, **kwargs):
    """Wrapper around :func:`sanic.response.json` that uses a
    custom JSON encoder for UUIDs."""

    resp = response.json(
        *args, **kwargs,
        dumps=lambda v: pyjson.dumps(v, cls=CustomJSONEncoder)
    )

    resp.sign_flag = True

    return resp
