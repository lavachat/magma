# Magma: LavaChat reference server
# Copyright 2018, LavaChat Team and the Magma contributors
# SPDX-License-Identifier: AGPL-3.0-only

from uuid import UUID

from logbook import Logger
from cerberus import Validator

from magma.errors import BadRequest

log = Logger(__name__)


def validate(data, schema: dict, raise_err: bool = True):
    """Validate a single document against a given schema.

    Schema writing can be looked on the Cerberus package
    documentation.

    Parameters
    ----------
    data: Union[dict, list]
        Data to be validated.
    schema: dict
        The schema that data will be validated against.
    raise_err: bool, optional
        If this will raise a BadRequest error or None.
        Default is true.

    Returns
    -------
    Union[dict, list]
        A validated document.

    Raises
    ------
    BadRequest
        If ``raise_err`` is true.
    Exception
        On validation errors.
    """
    validator = Validator(schema)

    try:
        valid = validator.validate(data)
    except Exception as err:
        log.exception('error while validating')
        raise err

    if not valid:
        errors = validator.errors
        log.warning('validation err: in={} errs={}', data, errors)

        if raise_err:
            raise BadRequest('bad payload', {
                'errors': errors
            })

        return None

    # this has the 'default' fields set properly.
    return validator.document


class AuthSchemas:
    """Schemas related to authentication."""
    REGISTER = {
        # TODO: username and email dedicated types
        'username': {'type': 'string', 'required': True},
        'email': {'type': 'string', 'required': True},
        'password': {
            'type': 'string', 'minlength': 5, 'maxlength': 128,
            'required': True
        }
    }

    LOGIN_PLAIN = {
        'username': {'type': 'string', 'required': True},
        'password': {'type': 'string', 'required': True}
    }


class UserSchemas:
    EXTERNAL_USER = {
        # TODO: make a nickname type
        # TODO: aliases (list of useralias, make useralias type too)
        # TODO: avatars
        'id': {'coerce': UUID, 'required': True},
        'nick': {'type': 'string', 'required': True},
        'bot': {'type': 'boolean', 'required': True},
    }

    PROFILE = {
        # TODO: username, nickname, password, email types
        'username': {'type': 'string', 'required': False},
        'nickname': {'type': 'string', 'required': False},
        'bot': {'type': 'boolean', 'required': False},
        'password': {'type': 'string', 'required': False},

        # to change email, you need password
        'email': {
            'type': 'string', 'required': False,
            'dependencies': 'password'
        },

    }
