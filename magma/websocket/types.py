# Magma: LavaChat reference server
# Copyright 2018, LavaChat Team and the Magma contributors
# SPDX-License-Identifier: AGPL-3.0-only

from enum import Enum

class Encoding(Enum):
    """Encoding for messages."""
    json = 'json'
    msgpack = 'msgpack'


class Compression(Enum):
    """Compression schemes in a websocket."""
    none = None
    zlib = 'zlib'
    zlib_stream = 'zlib-stream'


class OP:
    """OP codes for a websocket connection."""
    identify = 0
    resume = 1
    connected = 2
    resumed = 3
    resume_fail = 4
    dispatch = 5
    heartbeat = 10
    heartbeat_ack = 11
    cap_neg_fail = 20
