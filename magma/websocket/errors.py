# Magma: LavaChat reference server
# Copyright 2018, LavaChat Team and the Magma contributors
# SPDX-License-Identifier: AGPL-3.0-only

class WebsocketError(Exception):
    """General websocket error"""
    close_code = 4000


class PayloadError(WebsocketError):
    """Payload is invalid."""
    close_code = 4001


class IdentifyFail(WebsocketError):
    """Failure while identifying"""
    close_code = 4002
