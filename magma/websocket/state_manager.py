# Magma: LavaChat reference server
# Copyright 2018, LavaChat Team and the Magma contributors
# SPDX-License-Identifier: AGPL-3.0-only

from collections import defaultdict

from magma.websocket.state import ConnState

class StateManager:
    """Holder class for all instances of :class:`ConnState`."""
    def __init__(self, app):
        self.app = app

        # this is a {user_id: {session_id: ConnState}}
        self.states = defaultdict(dict)

        # {session_id: ConnState} for fast queries
        # in get() calls
        self.states_by_sess_id = {}

    def get(self, session_id: str) -> ConnState:
        """Get a :class:`ConnState` via its session id."""
        return self.states_by_sess_id.get(session_id)

    def insert(self, state):
        """Insert a state into the manager"""
        user_id, sess_id = state.user_id, state.session_id
        self.states[user_id][sess_id] = state
        self.states_by_sess_id[sess_id] = state

    def remove(self, state):
        """Remove a state from the manager."""
        user_id, sess_id = state.user_id, state.session_id
        self.states[user_id].pop(sess_id, None)
        self.states_by_sess_id.pop(sess_id, None)
