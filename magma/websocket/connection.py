# Magma: LavaChat reference server
# Copyright 2018, LavaChat Team and the Magma contributors
# SPDX-License-Identifier: AGPL-3.0-only

import json

import msgpack
from logbook import Logger

from magma.websocket.types import Encoding, OP
from magma.common.auth import check_token

from magma.errors import BadRequest
from magma.websocket.errors import (
    WebsocketError, PayloadError, IdentifyFail
)

from magma.schemas import validate
from magma.response import CustomJSONEncoder
from magma.websocket.state import ConnState

log = Logger(__name__)


def _custom_json_dumps(data):
    return json.dumps(data, cls=CustomJSONEncoder,
                      separators=(',', ':'))


def _custom_msgpack_dumps(data):
    encoder = CustomJSONEncoder()
    msgpack.dumps(data, default=encoder.default)


ENCODERS = {
    Encoding.json: _custom_json_dumps,
    Encoding.msgpack: _custom_msgpack_dumps
}

DECODERS = {
    Encoding.json: json.loads,
    Encoding.msgpack: msgpack.loads
}

INCOMING_PACKET_SCHEMA = {
    'op': {'coerce': int},
    'd': {},
}

class WSConnection:
    """Represents a C2S connection."""
    def __init__(self, app, ws, encoding, compression):
        self.app = app
        self.ws = ws
        self.encoding = encoding
        self.compression = compression
        self.state = None

    def _encode(self, payload) -> str:
        encoder = ENCODERS[self.encoding]
        return encoder(payload)

    def _decode(self, data):
        return DECODERS[self.encoding](data)

    async def send(self, payload):
        """Send a single payload across the websocket."""
        # encode and compress it
        encoded = self._encode(payload)

        # TODO: compressing
        await self.ws.send(encoded)

    async def send_op(self, op_code: int, data):
        """Send an OP code with data to the client."""
        await self.send({
            'op': op_code,
            'd': data
        })

    async def recv(self):
        """Receive a single payload from the websocket."""
        packet = await self.ws.recv()

        try:
            decoded = self._decode(packet)
        except ValueError:
            raise PayloadError('Error decoding message')

        try:
            payload = validate(decoded, INCOMING_PACKET_SCHEMA, False)
        except BadRequest:
            raise PayloadError('Payload does not validate')

        return payload

    async def _mk_users_lookup(self, rels: list, rel_reqs: list):
        """Make a single list containing users for the client's user cache."""

        rel_ids = set(r['peer_id'] for r in rels)
        rel_req_ids = set(r['peer_id'] for r in rel_reqs)
        uids = rel_ids | rel_req_ids

        res = []

        for uid in uids:
            user = await self.app.storage.user.get(uid)
            res.append(user)

        return res

    async def _identify(self, data: dict):
        """Identify logic."""
        token = data['token']

        # TODO: capability negotiation
        uid = await check_token(self.app, token)

        if uid is None:
            raise IdentifyFail('Invalid token')

        # generate connection state
        self.state = ConnState(self, user_id=uid)

        # get user relationships
        rels = await self.app.storage.user.get_relationships(uid)
        rel_reqs = await self.app.storage.user.get_rel_reqs(uid)
        users_lookup = await self._mk_users_lookup(rels, rel_reqs)

        await self.send_op(OP.connected, {
            # TODO: higher / random hb_tp
            'hb_tp': 5000,
            'session_id': self.state.session_id,

            'user': await self.app.storage.user.get_private(uid),
            'relationships': rels,
            'rel_requests': rel_reqs,
            'users': users_lookup,

            'capabilities': [],
        })

    async def _identify_or_resume(self):
        """Handle an Identify OR Resume packet from the connection."""
        start_msg = await self.recv()
        start_op = start_msg['op']

        if start_op == OP.identify:
            await self._identify(start_msg['d'])
        elif start_op == OP.resume:
            raise NotImplementedError
        else:
            raise PayloadError('can only identify/resume')

    async def _recv_step(self):
        payload = await self.recv()
        payload_op = payload['op']

        try:
            handler = getattr(self, f'_handle_{payload_op}')
        except AttributeError:
            raise PayloadError('invalid op code')

        await handler(payload['d'])

    async def loop(self):
        """Start the websocket in a loop."""
        try:
            await self._identify_or_resume()

            while True:
                await self._recv_step()
        except WebsocketError as err:
            await self.ws.close(err.close_code,
                                reason=err.args[0])
        except Exception as err:
            log.exception('error on websocket loop')
            await self.ws.close(4000,
                                reason=repr(err))
