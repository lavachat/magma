# Magma: LavaChat reference server
# Copyright 2018, LavaChat Team and the Magma contributors
# SPDX-License-Identifier: AGPL-3.0-only

import os
import hashlib

def _gen_sess_id() -> str:
    return hashlib.md5(os.urandom(128)).hexdigest()[:32]

class ConnState:
    """Websocket state class."""
    def __init__(self, conn, **kwargs):
        self.conn = conn
        self.user_id = kwargs['user_id']
        self.session_id = _gen_sess_id()
