# Magma: LavaChat reference server
# Copyright 2018, LavaChat Team and the Magma contributors
# SPDX-License-Identifier: AGPL-3.0-only

class MagmaError(Exception):
    """Base error class."""
    status_code = 500


class BadRequest(MagmaError):
    """Bad client request input."""
    status_code = 400


class ExternalConnectionError(BadRequest):
    """Instance not found"""
    error_code = 200


class MethodNotAllowed(MagmaError):
    """Error when something is purposefully disabled."""
    status_code = 405


class AuthFailure(MagmaError):
    """Any kind of authentication failure."""
    status_code = 401


class NotFound(MagmaError):
    """Resource not found"""
    status_code = 404


class UserNotFound(NotFound):
    """User not Found"""
    error_code = 100


class SignatureError(MagmaError):
    """Signature error"""
    status_code = 400
    error_code = 201


class ServerBlocked(MagmaError):
    """S2S block"""
    status_code = 400
    error_code = 202


class PeerBlocked(ServerBlocked):
    """Blocked by our configuration"""
    error_code = 203
