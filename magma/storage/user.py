# Magma: LavaChat reference server
# Copyright 2018, LavaChat Team and the Magma contributors
# SPDX-License-Identifier: AGPL-3.0-only

import asyncpg

from magma.errors import UserNotFound, BadRequest

class UserStorage:
    """Storage functions for users."""
    def __init__(self, storage):
        self.app = storage.app
        self.db = storage.app.db

    async def get_private(self, user_id: str) -> dict:
        """Get a PrivateUser. Only works if the user is local."""

        row = await self.db.fetchrow("""
        SELECT id, username, nickname, bot,
               email, verified
        FROM users
        WHERE id = $1
        """, user_id)

        if row is None:
            raise UserNotFound()

        return dict(row)

    async def get(self, user_id: str) -> dict:
        """Get a single user's information as a dictionary"""

        if '@' not in user_id:
            raise BadRequest('Invalid UserID')

        row = await self.db.fetchrow(f"""
        SELECT id, username, nickname, bot
        FROM users
        WHERE id = $1
        """, user_id)

        if row is None:
            raise UserNotFound()

        drow = dict(row)

        if drow['nickname'] is None:
            drow['nickname'] = drow['username']

        drow.pop('username')

        return dict(row)

    async def insert_external(self, user: dict):
        """Insert an external user into our database."""
        try:
            await self.db.execute("""
            INSERT INTO users (id, nickname, bot)
            VALUES ($1, $2, $3)
            """, user['id'], user['nick'], user['bot'])
        except asyncpg.UniqueViolationError:
            # if a user with the same lc_id already exists, we
            # should just ignore the incoming external user.
            pass

    async def get_relationships(self, user_id) -> list:
        """Get relationships for a user."""
        rows = await self.db.fetch("""
        SELECT peer_id
        FROM direct_relationships
        WHERE user_id = $1
        """, user_id)

        drows = [dict(row) for row in rows]

        for row in drows:
            row['type'] = 0 if row['rel_type'] == 3 else 1
            row.pop('rel_type')

        return drows

    async def get_rel_reqs(self, user_id) -> list:
        """Get relationship requests for a user."""
        rows = await self.db.fetch("""
        SELECT peer_id
        FROM friend_requests
        WHERE user_id = $1
        """, user_id)

        drows = [dict(row) for row in rows]

        for row in drows:
            row['type'] = row['rel_type']
            row.pop('rel_type')

        return drows
