# Magma: LavaChat reference server
# Copyright 2018, LavaChat Team and the Magma contributors
# SPDX-License-Identifier: AGPL-3.0-only

import aiohttp
from logbook import Logger

from cryptography.hazmat.backends import default_backend
from cryptography.hazmat.primitives import serialization
from cryptography.hazmat.primitives.serialization import (
    Encoding, PublicFormat, PrivateFormat, NoEncryption
)

log = Logger(__name__)


def to_external_pub(key) -> str:
    """Convert a public key to PEM."""
    return key.public_bytes(
        encoding=Encoding.PEM,
        format=PublicFormat.SubjectPublicKeyInfo
    ).decode()


class KeyStorage:
    """Key storage functions."""
    def __init__(self, storage):
        self.app = storage.app
        self.db = storage.app.db
        self.session = aiohttp.ClientSession()
        self._load_keys()

    def _load_keys(self):
        if 'secrets' not in self.app.config:
            log.warning('no secrets file loaded, expect errors.')
            return

        secrets = self.app.config['secrets']

        self._sign_key = serialization.load_pem_private_key(
            secrets['signing_key'].encode(),
            password=None,
            backend=default_backend()
        )

    async def _insert_null_key(self, instance: str):
        log.debug('inserting NULL key to {!r}', instance)

        await self.db.execute("""
        INSERT INTO instance_keys
            (instance, key, key_type, expires_at)
        VALUES
            (
                $1, NULL, 'invalid',
                EXTRACT(
                    epoch from (now() + interval '5 minutes')
                )
            )
        """, instance)

    async def _fetch_key(self, instance):
        await self.db.execute("""
        DELETE FROM instance_keys
        WHERE instance = $1
        """, instance)

        # query the instance's keys
        url = f'https://{instance}/_/lavachat/keys/@self'

        log.debug('querying keys for {!r}', instance)

        try:
            async with self.session.get(url) as resp:
                if resp.status != 200:
                    await self._insert_null_key(instance)
                    return

                rjson = await resp.json()
        except aiohttp.ClientError:
            await self._insert_null_key(instance)
            return

        ttl = int(rjson['ttl'])

        log.debug('inserted key {!r} {}s ttl => {!r}',
                  instance, ttl, rjson['key'])

        await self.db.execute(f"""
        INSERT INTO instance_keys
            (instance, key, key_type, expires_at)
        VALUES
            ($1, $2, $3,
                EXTRACT(epoch from (now() + interval '{ttl} seconds'))
            )
        """, instance, rjson['key'], 'rsa')

    async def get_instance_key(self, instance: str):
        """Get the main signing key for an instance.

        Returns
        -------
        dict
            If a key is found, with `type` and `key` fields.
        None
            If the key is not found, can be caused by a server not giving
            anything on its /_/lavachat/keys/@self path.
        """
        row = await self.db.fetchrow("""
        SELECT key, key_type
        FROM instance_keys
        WHERE instance = $1
          AND EXTRACT(epoch from now()) < expires_at
        """, instance)

        if row is None:
            await self._fetch_key(instance)
            return await self.get_instance_key(instance)

        if row['key'] is None:
            return None

        return dict(row)

    @property
    def self_signing(self):
        """Get our signing key."""
        if not self._sign_key:
            self._load_keys()

        return self._sign_key

    @property
    def self_signing_pem(self):
        return self.self_signing.private_bytes(
            encoding=Encoding.PEM,
            format=PrivateFormat.PKCS8,
            encryption_algorithm=NoEncryption()
        )

    @property
    def self_signing_pub(self):
        """Get signing public key."""
        signing_key = self.self_signing
        return signing_key.public_key()
