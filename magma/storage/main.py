# Magma: LavaChat reference server
# Copyright 2018, LavaChat Team and the Magma contributors
# SPDX-License-Identifier: AGPL-3.0-only

from magma.storage.user import UserStorage
from magma.storage.keys import KeyStorage

class Storage:
    """Main storage holder class."""
    def __init__(self, app):
        self.app = app
        self.db = app.db

        self.user = UserStorage(self)
        self.keys = KeyStorage(self)
