# Magma: LavaChat reference server
# Copyright 2018, LavaChat Team and the Magma contributors
# SPDX-License-Identifier: AGPL-3.0-only

import asyncio
from typing import Union

import bcrypt
from itsdangerous import TimestampSigner, BadSignature

from magma.errors import AuthFailure


async def hash_data(data: Union[str, bytes], loop=None) -> str:
    """Hash any data with bcrypt."""
    if isinstance(data, str):
        data = data.encode()

    loop = loop or asyncio.get_event_loop()

    hashed = await loop.run_in_executor(
        None, bcrypt.hashpw, data, bcrypt.gensalt(14)
    )

    return hashed.decode()

async def hash_check(stored: str, data: str, loop=None):
    """Compare if data hashes to the given stored hash."""
    stored = stored.encode()
    data = data.encode()
    loop = loop or asyncio.get_event_loop()

    success = await loop.run_in_executor(
        None, bcrypt.checkpw, data, stored
    )

    if not success:
        raise AuthFailure('Invalid password')


async def check_token(app, token: str) -> str:
    """Check a token's information."""
    local_id = token.split('.')[0]

    row = await app.db.fetchrow("""
    SELECT id, password_hash
    FROM users
    WHERE local_id = $1
    """, local_id)

    user_id, phash = row

    signer = TimestampSigner(phash)

    try:
        signer.unsign(token)
        return user_id
    except BadSignature:
        return False


async def validate_token(app, token: str):
    """Validate a token."""
    if not check_token(app, token):
        raise AuthFailure('Invalid token')
