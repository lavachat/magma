# Magma: LavaChat reference server
# Copyright 2018, LavaChat Team and the Magma contributors
# SPDX-License-Identifier: AGPL-3.0-only

import uuid

from magma.common.auth import hash_data

async def register_user(app, username: str, email: str,
                        password: str) -> uuid.UUID:
    """Register a single user."""
    local_id = uuid.uuid4()

    password_hash = await hash_data(password)

    user_id = f'@{local_id.hex}:{app.config.magma["url"]}'

    await app.db.execute("""
    INSERT INTO users
        (id, local_id, username, email, password_hash)
    VALUES
        ($1, $2, $3, $4, $5)
    """, user_id, local_id, username, email, password_hash)

    return user_id
