# Magma: LavaChat reference server
# Copyright 2018, LavaChat Team and the Magma contributors
# SPDX-License-Identifier: AGPL-3.0-only

from httpsig import HeaderVerifier

from magma.common.auth import check_token
from magma.errors import AuthFailure

def auth_route(handler):
    """Declare a request handler as requiring the
    Authorization header with a valid token."""
    async def _new_handler(request, *args, **kwargs):
        token = request.token
        if not token:
            raise AuthFailure('no token provided')

        user_id = await check_token(request.app, token)

        if not user_id:
            raise AuthFailure('invalid token')

        return await handler(request, user_id, *args, **kwargs)

    return _new_handler


def s2s_in(handler):
    """Mark a route as a S2S-in route.

    It will validate the request's signature before
    carrying on with handler logic.
    """
    async def _new_handler(request, *args, **kwargs):
        peer_domain = request.headers['x-lavachat-origin']
        app = request.app

        peer_key = await app.storage.keys.get_instance_key(peer_domain)

        if peer_key is None:
            raise AuthFailure('Keys not found for lavachat origin server.')

        verifier = HeaderVerifier(
            request.headers, peer_key['key'], method=request.method,
            path=request.path, host=request.headers['host'],
            sign_header='signature',
        )

        if not verifier.verify():
            raise AuthFailure('Signature failure')

        await app.federator.is_blocked(peer_domain, raise_err=False)
        return await handler(request, peer_domain, *args, **kwargs)

    return _new_handler
