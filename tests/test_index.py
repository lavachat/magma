# Magma: LavaChat reference server
# Copyright 2018, LavaChat Team and the Magma contributors
# SPDX-License-Identifier: AGPL-3.0-only

async def test_index(test_cli):
    resp = await test_cli.get('/')
    assert resp.status == 200
