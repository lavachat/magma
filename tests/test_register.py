# Magma: LavaChat reference server
# Copyright 2018, LavaChat Team and the Magma contributors
# SPDX-License-Identifier: AGPL-3.0-only

from tests.utils import rand_hex

async def test_register(app, test_cli):
    """Test registration"""

    rand_uname = rand_hex()

    resp = await test_cli.post('/api/auth/register', json={
        'username': rand_uname,
        'email': f'{rand_hex(5)}@{rand_hex(5)}.com',
        'password': rand_hex(),
    })

    assert resp.status == 200
    rjson = await resp.json()
    assert isinstance(rjson['id'], str)

    # delete the user soon after
    await app.db.execute("""
    DELETE FROM users
    WHERE username = $1
    """, rand_uname)
