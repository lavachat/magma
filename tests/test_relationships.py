# Magma: LavaChat reference server
# Copyright 2018, LavaChat Team and the Magma contributors
# SPDX-License-Identifier: AGPL-3.0-only

from tests.utils import login_normal

async def test_listing(test_cli):
    utoken = await login_normal(test_cli)
    resp = await test_cli.get('/api/relationships/@me', headers={
        'Authorization': utoken
    })

    assert resp.status == 200
    rjson = await resp.json()
    assert isinstance(rjson, list)


async def test_list_requests(test_cli):
    utoken = await login_normal(test_cli)
    resp = await test_cli.get('/api/relationships/@me/requests', headers={
        'Authorization': utoken
    })

    assert resp.status == 200
    rjson = await resp.json()
    assert isinstance(rjson, list)
