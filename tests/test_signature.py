# Magma: LavaChat reference server
# Copyright 2018, LavaChat Team and the Magma contributors
# SPDX-License-Identifier: AGPL-3.0-only

from httpsig import HeaderVerifier


async def test_signature_match(test_cli):
    """Test if the signature given in a request matches."""

    # /keys/@self contains both the signing key and
    # the response is itself signed so we can
    # check data.
    resp = await test_cli.get('/_/lavachat/keys/@self')
    assert resp.status == 200

    rjson = await resp.json()

    # extract @self rsa pub key
    key = rjson['key'].encode()

    verifier = HeaderVerifier(
        resp.headers, key, method='GET', path='/_/lavachat/keys/@self',
        host=resp.headers['host'], sign_header='signature'
    )

    assert verifier.verify()
