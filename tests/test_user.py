# Magma: LavaChat reference server
# Copyright 2018, LavaChat Team and the Magma contributors
# SPDX-License-Identifier: AGPL-3.0-only

import uuid
from tests.utils import login_normal, fetch_profile

async def test_user_self(test_cli):
    utoken = await login_normal(test_cli)
    resp = await test_cli.get('/api/users/@me', headers={
        'Authorization': utoken
    })

    assert resp.status == 200

    rjson = await resp.json()

    assert isinstance(rjson['id'], str)
    assert isinstance(rjson['username'], str)
    assert isinstance(rjson['bot'], bool)

async def test_user_full(test_cli):
    user_id, utoken = await login_normal(test_cli, uid=True)

    resp = await test_cli.get(f'/api/users/{user_id}', headers={
        'Authorization': utoken
    })

    assert resp.status == 200

    rjson = await resp.json()

    assert isinstance(rjson['id'], str)
    assert isinstance(rjson['username'], str)
    assert isinstance(rjson['bot'], bool)

async def test_user_invalid(test_cli):
    utoken = await login_normal(test_cli)

    resp = await test_cli.get('/api/users/larmge_tiddy', headers={
        'Authorization': utoken
    })

    assert resp.status == 400


async def _try_search(utoken, test_cli, query) -> tuple:
    resp = await test_cli.get(f'/api/users/search?q={query}', headers={
        'Authorization': utoken
    })

    return resp.status, resp


async def test_invalid_users(test_cli):
    ruuid = uuid.uuid4()
    ruuid = ruuid.hex

    inst_url = test_cli._app.config.magma['url']

    to_discover = [
        f'@{ruuid}:example.com',
        f'@{ruuid}:{inst_url}',
        f'{ruuid}@example.com',
        f'{ruuid}@{inst_url}',
        f'@{ruuid}',
        f'{ruuid}'
    ]

    utoken = await login_normal(test_cli)

    for query in to_discover:
        status, _ = await _try_search(utoken, test_cli, query)
        assert status in (400, 404)


async def test_patch_profile(test_cli):
    utoken = await login_normal(test_cli)

    # lmao
    random_nick = uuid.uuid4().hex[:10]

    resp = await test_cli.patch('/api/users/@me',
        json={
            'nickname': random_nick
        }, headers={
            'Authorization': utoken
        }
    )

    assert resp.status == 200

    # ensure changes went through
    resp, profile = await fetch_profile(test_cli, utoken)
    assert resp.status == 200
    assert profile['nickname'] == random_nick
