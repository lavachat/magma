import json

from tests.utils import login_normal
from magma.websocket.types import OP

async def _send(conn, obj):
    data = json.dumps(obj)
    await conn.send_str(data)

async def _recv(conn):
    msg = await conn.receive()
    return json.loads(msg.data)

async def test_websocket(test_cli):
    """Test websocket"""
    utoken = await login_normal(test_cli)
    ws = await test_cli.ws_connect('/api/ws')

    await _send(ws, {
        'op': OP.identify,
        'd': {
            'token': utoken,
            'caps': []
        }
    })

    payload = await _recv(ws)
    data = payload['d']

    assert payload['op'] == OP.connected
    assert isinstance(data['hb_tp'], int)
    assert isinstance(data['session_id'], str)
    assert isinstance(data['user'], dict)
    assert isinstance(data['relationships'], list)
    assert isinstance(data['rel_requests'], list)
    assert isinstance(data['users'], list)
    assert isinstance(data['capabilities'], list)
