# Magma: LavaChat reference server
# Copyright 2018, LavaChat Team and the Magma contributors
# SPDX-License-Identifier: AGPL-3.0-only

async def test_wellknown(test_cli):
    """Test the well-known URL."""
    resp = await test_cli.get('/.well-known/lavachat')
    assert resp.status == 200
    rjson = await resp.json()

    assert 'c2s_apis' in rjson
    assert 's2s_apis' in rjson
