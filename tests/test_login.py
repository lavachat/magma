# Magma: LavaChat reference server
# Copyright 2018, LavaChat Team and the Magma contributors
# SPDX-License-Identifier: AGPL-3.0-only

from tests.utils import login_normal

async def test_login(test_cli):
    """Test a login with one of the default test users."""
    token = await login_normal(test_cli)
    assert isinstance(token, str)
