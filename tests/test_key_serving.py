# Magma: LavaChat reference server
# Copyright 2018, LavaChat Team and the Magma contributors
# SPDX-License-Identifier: AGPL-3.0-only

async def test_self_keys(test_cli):
    """Test if the route to get the keys on the self server work."""
    resp = await test_cli.get('/_/lavachat/keys/@self')
    assert resp.status == 200
    rjson = await resp.json()

    assert 'ttl' in rjson
    assert 'key' in rjson
