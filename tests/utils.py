# Magma: LavaChat reference server
# Copyright 2018, LavaChat Team and the Magma contributors
# SPDX-License-Identifier: AGPL-3.0-only

import secrets
from tests.creds import CREDS


def rand_hex(length=10) -> str:
    """Random hex string"""
    return secrets.token_hex(length)


async def login_normal(test_cli, *, uid=False):
    username, password = CREDS['normal']

    resp = await test_cli.post('/api/auth/login/plain', json={
        'username': username,
        'password': password,
    })

    assert resp.status == 200

    rjson = await resp.json()

    if uid:
        user_id = await test_cli._app.db.fetchval("""
        SELECT id
        FROM users
        WHERE username = $1
        """, username)

        return user_id, rjson['token']

    return rjson['token']


async def fetch_profile(test_cli, utoken: str):
    """Fetch PrivateUser."""
    resp = await test_cli.get('/api/users/@me', headers={
        'Authorization': utoken
    })

    return resp, await resp.json()
