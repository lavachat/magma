# Magma: LavaChat reference server
# Copyright 2018, LavaChat Team and the Magma contributors
# SPDX-License-Identifier: AGPL-3.0-only

import os
import sys

import pytest
from sanic.websocket import WebSocketProtocol

# hacky!
sys.path.append(os.getcwd())
from run import make_app


@pytest.yield_fixture
def app():
    """Yield an app instance."""
    app_ = make_app()
    yield app_


@pytest.fixture
def test_cli(loop, app, test_client):
    """Return a test client."""
    return loop.run_until_complete(test_client(app, protocol=WebSocketProtocol))
