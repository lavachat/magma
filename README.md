# Magma

LavaChat reference implementation.

[LavaChat] provides the federation protocol.

## Goals

 - work

## Installing

Requirements:
 - **Python 3.7**
 - PostgreSQL 9.6+
 - [Pipenv]
 - libraries required by [cryptography]

[pipenv]: https://pipenv.readthedocs.io/en/latest/install
[cryptography]: https://cryptography.io/en/latest/installation/#installation
[lavachat]: https://gitlab.com/lavachat/docs

```sh
git clone https://gitlab.com/lavachat/magma && cd magma
```

### Download dependencies

```sh
$ pipenv install
```

### Setup the database

This is just an example of setting the user. YMMV.

```sql
$ sudo -u postgres psql

-- make sure to put the credentials when configuring 
CREATE USER magma WITH PASSWORD 'secure password';
CREATE DATABASE magma;
GRANT ALL PRIVILEGES ON DATABASE magma TO magma;
```

```sh
# load tables in
$ psql -U magma -f schema.sql
```

### Configuration

```sh
$ cp config/config.example.yml config/config.yml

# edit as you wish
$ $EDITOR config/config.yml
```

### Running

```sh
pipenv run python run.py
```
